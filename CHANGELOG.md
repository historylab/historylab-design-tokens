# Changelog
## [0.3.0] 2018-04-20
### Changed
* renamed `font-size-sans-serif` to `font-size-sans`
## [0.2.0] 2018-04-20
### Changed
* simplified notation thanks to removing arrays
* renamed `font-family-sans-serif` to `font-family-sans`